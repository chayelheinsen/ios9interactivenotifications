//
//  InteractiveNotification.swift
//  InteractiveNotifications
//
//  Created by Chayel Heinsen  on 6/12/15.
//  Copyright © 2015 Chayel Heinsen. All rights reserved.
//

import UIKit

class InteractiveNotification: NSObject {
    
    // MARK: - Properties -
    // MARK: Private
    
    private var categories: Set<UIUserNotificationCategory> = Set()
    
    // MARK: Public
    
    // MARK: - Methods -
    
    // MARK: - Class -
    // MARK: Private
    
    
    // MARK: Public
    
    
    // MARK: - Instance -
    // MARK: Private
    
    
    // MARK: Public
    
    func action(identifier identifier: String, title: String, activationMode: UIUserNotificationActivationMode, behavior: UIUserNotificationActionBehavior) -> UIMutableUserNotificationAction {
        let action = UIMutableUserNotificationAction()
        action.identifier = identifier
        action.title = title
        action.activationMode = activationMode
        action.authenticationRequired = false
        action.destructive = false
        action.behavior = behavior
        return action
    }
    
    func addCategory(identifier identifier: String, actions: [UIMutableUserNotificationAction]) {
        let category = UIMutableUserNotificationCategory()
        category.identifier = identifier
        category.setActions(actions, forContext: .Default)
        category.setActions(actions, forContext: .Minimal)
        categories.insert(category)
    }
    
    func registerNotifications() {
        let settings = UIUserNotificationSettings(forTypes: [.Alert, .Badge, .Sound], categories: categories)
        UIApplication.sharedApplication().registerUserNotificationSettings(settings)
    }
    
}
