//
//  AppDelegate.swift
//  iOS9InteractiveNotifications
//
//  Created by Chayel Heinsen  on 6/12/15.
//  Copyright © 2015 Chayel Heinsen. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        
        // Create a InteractiveNotification object
        let notification: InteractiveNotification = InteractiveNotification()
        
        // Create actions to use with the notification
        
        // This is a text input
        let action1: UIMutableUserNotificationAction = notification.action(identifier: "ACTION", title: "Reply", activationMode: .Background, behavior: .TextInput)
        
        // This is a button
        let action2: UIMutableUserNotificationAction = notification.action(identifier: "ACTION2", title: "Yes", activationMode: .Background, behavior: .Default)
        let action3: UIMutableUserNotificationAction = notification.action(identifier: "ACTION3", title: "No", activationMode: .Background, behavior: .Default)
        
        // Add the actions to a category.
        notification.addCategory(identifier: "REPLY_CATEGORY", actions: [action1])
        
        // You can even add mutilple actions to the same category
        notification.addCategory(identifier: "BUTTON_CATEGORY", actions: [action2, action3])
        
        // register notifications and ask permission
        notification.registerNotifications()
        
        return true
    }
    
    // This method will be called when we receive a local notification
    func application(application: UIApplication, handleActionWithIdentifier identifier: String?, forLocalNotification notification: UILocalNotification, withResponseInfo responseInfo: [NSObject : AnyObject], completionHandler: () -> Void) {
        
        // this is action1
        if identifier == "ACTION" {
            // The text is stored in the responseInfo under the UIUserNotificationActionResponseTypedTextKey key:
            let reply = responseInfo[UIUserNotificationActionResponseTypedTextKey]
            print(reply, appendNewline: true)
        }
        
        // this is action2
        if identifier == "ACTION2" {
            // The text is stored in the responseInfo under the UIUserNotificationTextInputActionButtonTitleKey key:
            let button = responseInfo[UIUserNotificationTextInputActionButtonTitleKey]
            print(button, appendNewline: true)
        }
        
    }
    
    // This method will be called when we receive a remote notification
    func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject]) {
        
    }
    
    func scheduleNotification() {
        let now: NSDateComponents = NSCalendar.currentCalendar().components([.Hour, .Minute], fromDate: NSDate())
        
        let cal = NSCalendar(calendarIdentifier: NSCalendarIdentifierGregorian)!
        let date = cal.dateBySettingHour(now.hour, minute: now.minute + 1, second: 0, ofDate: NSDate(), options: NSCalendarOptions())
        let reminder = UILocalNotification()
        reminder.fireDate = date
        reminder.alertBody = "You can now reply with text"
        reminder.alertAction = "Cool"
        reminder.soundName = "sound.aif"
        reminder.category = "REPLY_CATEGORY"
        
        UIApplication.sharedApplication().scheduleLocalNotification(reminder)
        
        print("Firing at \(now.hour):\(now.minute + 1)")
    }
}

